package gosmooth

type Declaration struct {
	TemplatePath     string
	DocumentType     string
	DocumentTitle    string
	ModuleTitle      string
	StaffName        string
	SignDate         string
	IsBachelorThesis bool
}
