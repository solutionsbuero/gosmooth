package gosmooth

type Metadata struct {
	Template      string `json:"template"`
	Engine        string `json:"engine"`
	PandocOptions string `json:"pandoc-options"`
	Bibliography  string `json:"bibliography"`
	Characters    string `json:"characters"`
	DoM4          bool   `json:"do-m4"`
	BreakVerses   bool   `json:"break-verses"`
}
