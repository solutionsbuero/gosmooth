package gosmooth

type Document struct {
	Path           string `json:"path"`
	InputPath      string `json:"input-path"`
	WorkDir        string `json:"work-dir"`
	Template       string `json:"template"`
	Engine         string `json:"engine"`
	PandocOptions  string `json:"pandoc-options"`
	DoBibliography bool   `json:"bibliography"`
	BibliographyDb string `json:"bibliography_db"`
	DoM4           bool   `json:"do-m4"`
	DoCharacters   bool
	BreakVerses    bool `json:"break-verses"`
}

func NewDocument(path, workDir string) Document {
	return Document{
		Path:      path,
		InputPath: path,
		WorkDir:   workDir,
	}
}
