package gosmooth

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"regexp"
	"strings"
)

type Smooth struct {
	Document Document
}

func NewSmooth(path, workDir string) Smooth {
	return Smooth{Document: NewDocument(path, workDir)}
}

func (s *Smooth) Run(outputPath string) {
	s.PreRun()
	s.convert(outputPath)
	s.Cleanup()
}

func (s *Smooth) PreRun() {
	if s.Document.WorkDir != "" {
		err := os.Chdir(s.Document.WorkDir)
		if err != nil {
			fmt.Println(err)
		}
	}

	s.extractMetadata()
	if s.Document.DoM4 {
		s.doM4()
	}
	if s.Document.BreakVerses {
		s.insertBreaks()
	}
}

func (s *Smooth) DoM4() {
	if s.Document.DoM4 {
		s.doM4()
	}
}

// EXTRACT METADATA

func (s *Smooth) extractMetadata() {
	metadata := s.extractJson()
	s.metadataToDocument(metadata)
}

func (s *Smooth) extractJson() Metadata {
	filePath := "/tmp/gosmooth_metadata.pandoc-tpl"
	file, err := os.Create(filePath)
	if err != nil {
		fmt.Println(err.Error())
	}
	defer file.Close()

	_, err = io.WriteString(file, "$meta-json$")
	if err != nil {
		fmt.Println(err.Error())
	}

	jsonRaw, err := exec.Command("pandoc", "--template", filePath, s.Document.Path).Output()
	if err != nil {
		fmt.Println(err.Error())
	}

	err = os.Remove(filePath)
	if err != nil {
		fmt.Println(err)
	}

	metadata := Metadata{}
	err = json.Unmarshal(jsonRaw, &metadata)
	if err != nil {
		fmt.Println(err.Error())
	}
	return metadata
}

func (s *Smooth) metadataToDocument(metadata Metadata) {
	if metadata.Template != "" {
		s.Document.Template = metadata.Template
	} else {
		fmt.Println("No template specified")
		os.Exit(1)
	}

	if metadata.Engine != "" {
		s.Document.Engine = metadata.Engine
	} else {
		s.Document.Engine = "xelatex"
	}

	if metadata.Bibliography != "" {
		s.Document.DoBibliography = true
		s.Document.BibliographyDb = metadata.Bibliography
	}

	if metadata.Characters == "auto" {
		s.Document.DoCharacters = true
	}

	s.Document.PandocOptions = metadata.PandocOptions
	s.Document.DoM4 = metadata.DoM4
	s.Document.BreakVerses = metadata.BreakVerses
}

// M4

func (s *Smooth) doM4() {
	command := exec.Command("m4", s.Document.Path)
	var stdout bytes.Buffer
	command.Stdout = &stdout

	err := command.Run()
	if err != nil {
		fmt.Println(err)
	}

	// fmt.Println(stdout.String())

	s.saveStageFile(stdout.String(), "/tmp/smooth_m4_done.md")
}

// INSERT BREAKS

func (s *Smooth) insertBreaks() {
	file, err := os.Open(s.Document.Path)
	if err != nil {
		fmt.Println(err)
	}
	defer file.Close()

	raw, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}

	re := regexp.MustCompile(`([: ] {3}.*)\n`)
	text := re.ReplaceAllString(string(raw), "$1  \n")
	s.saveStageFile(text, "/tmp/smooth_breaks_done.md")
}

// CONVERT

func (s *Smooth) convert(outputPath string) {
	if outputPath == "" {
		outputPath = fmt.Sprintf("%s.pdf", strings.TrimSuffix(s.Document.InputPath, path.Ext(s.Document.InputPath)))
	}

	call := []string{
		s.Document.Path,
		"--template",
		s.Document.Template,
		"--pdf-engine",
		s.Document.Engine,
		"--wrap=preserve",
	}
	if s.Document.PandocOptions != "" {
		call = append(call, s.Document.PandocOptions)
	}
	if s.Document.DoBibliography {
		call = append(
			call,
			"--filter",
			"pandoc-citeproc",
			"--bibliography",
			s.Document.BibliographyDb)
	}
	call = append(call, "-o", outputPath)

	s.runPandoc(call)
}

func (s *Smooth) runPandoc(call []string) {
	command := exec.Command("pandoc", call...)

	var stderr bytes.Buffer
	command.Stderr = &stderr

	err := command.Run()
	if err != nil {
		fmt.Printf("Pandoc error\n%s\n%s", err, stderr.String())
	}
}

// CLEANUP

func (s *Smooth) Cleanup() {
	if s.Document.DoM4 {
		err := os.Remove("/tmp/smooth_m4_done.md")
		if err != nil {
			fmt.Println(err)
		}
	}
	if s.Document.BreakVerses {
		err := os.Remove("/tmp/smooth_breaks_done.md")
		if err != nil {
			fmt.Println(err)
		}
	}
}

// HELPERS

func (s *Smooth) saveStageFile(content, path string) {
	file, err := os.Create(path)
	if err != nil {
		fmt.Println(err.Error())
	}
	defer file.Close()

	_, err = io.WriteString(file, content)
	if err != nil {
		fmt.Println(err.Error())
	}
	s.Document.Path = path
}
