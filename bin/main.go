package main

import (
	"fmt"
	"github.com/urfave/cli"
	"gitlab.com/solutionsbuero/gosmooth"
	"os"
)

func main() {
	app := cli.NewApp()
	app.Name = "gosmooth"
	app.Description = "simple pandoc wrapper"
	app.Version = "0.4.1"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "work-dir, w",
			Usage: "Location of the working dir",
		},
	}
	app.Action = func(ctx *cli.Context) error {
		defaultRun(ctx)
		return nil
	}

	err := app.Run(os.Args)
	if err != nil {
		fmt.Println(err)
	}
}

func defaultRun(ctx *cli.Context) {
	smooth := gosmooth.NewSmooth(ctx.Args().Get(0), ctx.String("work-dir"))
	smooth.Run("")
}
